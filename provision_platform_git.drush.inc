<?php

/**
 * @file
 * Provides a pre-verify hook that will clone a site from Git
 * if the repo URL is specified.
 */

/**
 * Implements hook_drush_command().
 */
function provision_platform_git_drush_command() {
  $items['provision-git_pull'] = array(
    'description' => 'Git pull and verify a platform',
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_ROOT,
  );

  return $items;
}

/**
 * Implements the provision-
 */
function drush_provision_platform_git_provision_git_pull() {
  drush_errors_on();
  
  if (d()->type === 'platform' && provision_file()->exists(d()->root) && d()->deploy_from_git) {
    drush_log(dt("Updating from git"));
    $command = 'cd ' . d()->root . ' && git pull';
    
    drush_log(dt("Executing: " . $command));
    
    $output = exec($command, $output, $return_value);
    if ($return_value === 0) {
      drush_log(dt('Updated from Git successfully. Verifying platform...'), "success");
      drush_invoke_process('@' . d()->name, 'provision-verify');
    }
    else {
      return drush_set_error("GIT_PULL_FAILED", "Could not pull from repository. Output from Git: " . $output);
    }
  }
}

/**
 * Implements drush_hook_provision_verify_validate().
 *
 * This needs to be called in the validate step so that it runs before the
 * Makefile check and the check for a Drupal installation. Those operations,
 * very inconveniently, run in the same function.
 */
function drush_provision_platform_git_provision_verify_validate() {
  if (d()->type === 'platform') {
    if (!provision_file()->exists(d()->root)->status() && d()->deploy_from_git) {

      drush_log(dt("Platform path does not exist, cloning from Git"));
      $command = 'git clone --recursive -- ' . escapeshellarg(trim(d()->repo_url)) . ' ' . d()->root . ' && cd ' . d()->root . ' && git checkout ' . escapeshellarg(trim(d()->repo_branch)) ;
      drush_log(dt("Executing: " . $command));

      // Clone the repo.
      $output = exec($command, $output, $return_value);

      if ($return_value === 0) {
        drush_log(dt("Platform successfully cloned from Git"), 'success');
      }
      else {
        // We send back the full output if something went wrong.
        return drush_set_error("GIT_CLONE_FAILED", "Could not clone git repository. Output from Git: " . $output);
      }
    }
  }
}

/**
 * Register our directory as a place to find provision classes.
 */
function provision_platform_git_register_autoload() {
  static $loaded = FALSE;
  if (!$loaded) {
    $loaded = TRUE;
    provision_autoload_register_prefix('Provision_', dirname(__FILE__));
  }
}

/**
 * Implements hook_drush_init().
 */
function provision_platform_git_drush_init() {
  provision_platform_git_register_autoload();
}

/**
 *  Implements hook_provision_services().
 */
function provision_platform_git_provision_services() {
  provision_platform_git_register_autoload();
  return array('git' => NULL);
}
